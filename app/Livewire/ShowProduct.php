<?php

namespace App\Livewire;

use App\Actions\Webshop\AddProductVariantToCart;
use App\Models\Product;
use Laravel\Jetstream\InteractsWithBanner;
use Livewire\Component;

class ShowProduct extends Component
{
    use InteractsWithBanner;

    public $productId;

    public $variant;

    public $rules = [
        'variant' => ['required', 'exists:App\Models\ProductVariant,id']
    ];

    public function mount() 
    {
        $this->variant = $this->product->variants->value('id');
    }

    public function addToCard(AddProductVariantToCart $cart) 
    {
        $this->validate();

        $cart->add(
            variantId: $this->variant
        );

        $this->banner('Your product has been added to your cart.');

        $this->dispatch('productAddedToCart');
    }

    public function getProductProperty() 
    {
        return Product::findOrFail($this->productId);

    }

    public function render()
    {
        return view('livewire.product');
    }
}
