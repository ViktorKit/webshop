<?php

use App\Livewire\Cart;
use App\Livewire\ShowProduct;
use App\Livewire\StoreFront;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', StoreFront::class)->name('home');
Route::get('/product/{productId}', ShowProduct::class)->name('product');
Route::get('/cart', Cart::class)->name('cart');

// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified',
// ])->group(function () {
//     Route::get('/dashboard', function () {
//         return view('dashboard');
//     })->name('dashboard');
// });
